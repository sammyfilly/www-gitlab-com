---
layout: handbook-page-toc
title: "CSM/CSE Webinar Calendar"
---
# On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [CSM Handbook homepage](/handbook/customer-success/csm/) for additional CSM/CSE-related handbook pages.

Watch previously recorded webinars on our [YouTube Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kpczt4pRtyF147Uvn2bGGvq).

---

# Upcoming Webinars

We’d like to invite you to our free upcoming webinars and workshops in October.

If you know someone who might be interested in attending, feel free to share the registration links with them. Everyone is welcome, and we hope to see you there!

## October 2023

### AMER Time Zone Webinars

#### Intro to GitLab
##### October 3rd, 2023 at 9:00-10:00AM Pacific Time / 12:00-1:00PM Eastern Time

Are you new to GitLab? Join this webinar, where we will review what GitLab is, how it benefits you, and the recommended workflow to allow you to get the most out of the platform.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_bXLwncuxSuGn1b1CkIzybQ#/registration)

#### Intro to CI/CD
##### October 10th, 2023 at 9:00-10:00AM Pacific Time / 12:00-1:00PM Eastern Time

Come learn about what CI/CD is and how it can benefit your team. We will cover an overview of CI/CD and what it looks like in GitLab. We will also cover how to get started with your first CI/CD pipeline in GitLab and the basics of GitLab Runners.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_gd0y9qIcTE--RAosMmaoFQ#/registration)

#### Hands-On GitLab CI Workshop 
##### October 11th, 2023 at 9:00-10:30AM Pacific Time / 12:00-1:30PM Eastern Time

Join us for a hands-on GitLab CI workshop and learn how it can fit in your organization!

Learn how to build simple GitLab pipelines and work up to more advanced pipeline structures and workflows, including security scanning and compliance enforcement.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_214CXs_IQteqZJMgpusWVw#/registration)

#### Advanced CI/CD
##### October 17th, 2023 at 9:00-10:00AM Pacific Time / 12:00-1:00PM Eastern Time

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_D9Z3voAhRVy1p-rn7On4pg#/registration)

#### Hands-On GitLab CI Workshop for Jenkins Users
##### October 18th, 2023 at 9:00-11:00AM Pacific Time / 12:00-2:00PM Eastern Time

Join us for a hands-on GitLab CI workshop and learn how it can fit in your organization!

We will kick things off by going over the differences between CI/CD in Jenkins and GitLab, syntax requirements, advantages to using GitLab, and how you can achieve the same outcomes in GitLab. Getting started with CI/CD in GitLab will take a lot less time than tends to be required for Jenkins, and your users can stay in a single platform. 

We will then dive into how to build simple GitLab pipelines and work up to more advanced pipeline structures and workflows, including security scanning and compliance enforcement.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_SaGYcRbTTnulNK-tRLdBTQ#/registration)


#### Holistic Approach to Securing the Development Lifecycle
##### October 24th, 2023 at 9:00-10:00AM Pacific Time / 12:00-1:00PM Eastern Time

Join this one-hour webinar to gain a better understanding of how to successfully shift security left to find and fix security flaws during development, and to do so more easily and with greater visibility and control than typical approaches can provide.

In this session, we will go over the security and compliance features that are available with your GitLab Ultimate account, including:
- How to achieve comprehensive security scanning without introducing a multitude of tools and systems that expand your attack surface.
- How to secure and protect your cloud-native applications and IaC environments within existing DevOps workflows.
- How to use a single source of truth to improve collaboration between development and security.
- How to manage all of your software vulnerabilities in one place.
- How to set up compliance pipelines to provide consistent guardrails for developers, and ensure separation of duties

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_M95B_Z-vRISZ652n7_XmGA#/registration)

#### Hands-on Workshop: Security and Compliance in GitLab
##### October 25th, 2023 at 9:00-11:00AM Pacific Time / 12:00-2:00PM Eastern Time

In this workshop we will focus on how you can secure your application with GitLab. 

We will first take a look at how to apply scanners to your CICD pipelines in a hands-on exercise so that any vulnerabilities are caught as soon as the code is committed. 

Next, we will look at compliance frameworks and pipelines to show how you can ensure no one within your development teams is cutting corners and exposing your application.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_8jTIZyFdTDCh3jvAWckLpg#/registration)

#### DevSecOps/Compliance
##### October 31st, 2023 at 9:00-10:00AM Pacific Time / 12:00-1:00PM Eastern Time

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_hOI6GSkQSAK-feMJ4YVxww#/registration)

### EMEA Time Zone Webinars

#### Intro to GitLab
##### October 3rd, 2023 at 9:00-10:00AM UTC / 11:00AM-12:00PM CET

Are you new to GitLab? Join this webinar, where we will review what GitLab is, how it benefits you, and the recommended workflow to allow you to get the most out of the platform.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_Bgmla-23T0eKgH2e8maMyw#/registration)

#### Intro to CI/CD
##### October 10th, 2023 at 9:00-10:00AM UTC / 11:00AM-12:00PM CET

Come learn about what CI/CD is and how it can benefit your team. We will cover an overview of CI/CD and what it looks like in GitLab. We will also cover how to get started with your first CI/CD pipeline in GitLab and the basics of GitLab Runners.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_k-VsIi0oTGujfTYfUEFbmQ#/registration)

#### Hands-On GitLab CI Workshop 
##### October 11th, 2023 at 9:00-10:30AM UTC / 11:00AM-12:30PM CET

Join us for a hands-on GitLab CI workshop and learn how it can fit in your organization!

Learn how to build simple GitLab pipelines and work up to more advanced pipeline structures and workflows, including security scanning and compliance enforcement.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_ex5AiVtERbeL3THcpihvqQ#/registration)

#### Advanced CI/CD
##### October 17th, 2023 at 9:00-10:00AM UTC / 11:00AM-12:00PM CET

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_QId9jL33SECJMXS4vlqxKw#/registration)

#### Hands-On GitLab CI Workshop for Jenkins Users
##### October 18th, 2023 at 9:00-11:00AM UTC / 11:00AM-1:00PM CET

Join us for a hands-on GitLab CI workshop and learn how it can fit in your organization!

We will kick things off by going over the differences between CI/CD in Jenkins and GitLab, syntax requirements, advantages to using GitLab, and how you can achieve the same outcomes in GitLab. Getting started with CI/CD in GitLab will take a lot less time than tends to be required for Jenkins, and your users can stay in a single platform. 

We will then dive into how to build simple GitLab pipelines and work up to more advanced pipeline structures and workflows, including security scanning and compliance enforcement.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_pj8OvIQ8RwKHELv5C9DEVw#/registration)

#### Holistic Approach to Securing the Development Lifecycle
##### October 24th, 2023 9:00-10:00AM UTC / 11:00AM-12:00PM CET

Join this one-hour webinar to gain a better understanding of how to successfully shift security left to find and fix security flaws during development, and to do so more easily and with greater visibility and control than typical approaches can provide.

In this session, we will go over the security and compliance features that are available with your GitLab Ultimate account, including:
- How to achieve comprehensive security scanning without introducing a multitude of tools and systems that expand your attack surface.
- How to secure and protect your cloud-native applications and IaC environments within existing DevOps workflows.
- How to use a single source of truth to improve collaboration between development and security.
- How to manage all of your software vulnerabilities in one place.
- How to set up compliance pipelines to provide consistent guardrails for developers, and ensure separation of duties

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_xBCSUKGQTLyVBZhxcRowoQ#/registration)

#### Hands-on Workshop: Security and Compliance in GitLab
##### October 25th, 2023 at 9:00-11:00AM UTC / 11:00AM-1:00PM CET

In this workshop we will focus on how you can secure your application with GitLab. 

We will first take a look at how to apply scanners to your CICD pipelines in a hands-on exercise so that any vulnerabilities are caught as soon as the code is committed. 

Next, we will look at compliance frameworks and pipelines to show how you can ensure no one within your development teams is cutting corners and exposing your application.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_o9tQ9bYkTLWAM7zvxqZ87A#/registration)

#### DevSecOps/Compliance
##### October 31st, 2023 at 9:00-10AM UTC / 11:00AM-12:00PM CET

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_h48TyGjBROGwkt9tDIoZuA#/registration)

Check back later for more webinars! 
